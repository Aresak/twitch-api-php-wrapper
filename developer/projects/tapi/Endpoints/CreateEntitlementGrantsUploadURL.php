<?php

namespace TwitchAPI;

class CreateEntitlementGrantsUploadURL extends Endpoint {
  public RequiredScopes() {
    return "";
  }

  public RequestURL() {
    return "helix/entitlements/upload";
  }

  public CallEndpoint($mixedVariable) {

  }
}

?>
