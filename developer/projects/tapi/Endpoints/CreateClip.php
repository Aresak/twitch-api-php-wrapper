<?php

namespace TwitchAPI;

class CreateClip extends Endpoint {
  public RequiredScopes() {
    return "clips:edit";
  }

  public RequestURL() {
    return "helix/clips";
  }

  public CallEndpoint($mixedVariable) {

  }
}

?>
