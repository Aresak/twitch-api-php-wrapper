<?php

namespace TwitchAPI;

class GetExtensionAnalytics extends Endpoint {
  public RequiredScopes() {
    return "analytics:read:extensions";
  }

  public RequestURL() {
    return "helix/analytics/extensions";
  }

  public CallEndpoint($mixedVariable) {

  }
}

?>
