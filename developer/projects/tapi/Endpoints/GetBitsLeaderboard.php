<?php

namespace TwitchAPI;

class GetBitsLeaderboard extends Endpoint {
  public RequiredScopes() {
    return "bits:read";
  }

  public RequestURL() {
    return "helix/bits/leaderboard";
  }

  public CallEndpoint($mixedVariable) {

  }
}

?>
