<?php

namespace TwitchAPI;

class GetExtensionAnalytics extends Endpoint {
  public RequiredScopes() {
    return "user:read:broadcast";
  }

  public RequestURL() {
    return "helix/users/extensions";
  }

  public CallEndpoint($mixedVariable) {

  }
}

?>
