<?php

namespace TwitchAPI;

class GetStreamsMetadata extends Endpoint {
  public RequiredScopes() {
    return "";
  }

  public RequestURL() {
    return "helix/streams/metadata";
  }

  public CallEndpoint($mixedVariable) {

  }
}

?>
