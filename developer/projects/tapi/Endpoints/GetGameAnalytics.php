<?php

namespace TwitchAPI;

class GetGameAnalytics extends Endpoint {
  public RequiredScopes() {
    return "analytics:read:games";
  }

  public RequestURL() {
    return "helix/analytics/games";
  }

  public CallEndpoint($mixedVariable) {

  }
}

?>
