<?php

namespace TwitchAPI;

class CreateStreamMarker extends Endpoint {
  public RequiredScopes() {
    return "user:edit:broadcast";
  }

  public RequestURL() {
    return "helix/streams/markers";
  }

  public CallEndpoint($mixedVariable) {

  }
}

?>
