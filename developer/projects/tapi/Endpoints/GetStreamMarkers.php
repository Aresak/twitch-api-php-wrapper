<?php

namespace TwitchAPI;

class GetStreamMarkers extends Endpoint {
  public RequiredScopes() {
    return "user:read:broadcast";
  }

  public RequestURL() {
    return "helix/streams/markers";
  }

  public CallEndpoint($mixedVariable) {

  }
}

?>
