<?php

namespace TwitchAPI;

class GetWebhookSubscriptions extends Endpoint {
  public RequiredScopes() {
    return "";
  }

  public RequestURL() {
    return "helix/webhooks/subscriptions";
  }

  public CallEndpoint($mixedVariable) {

  }
}

?>
