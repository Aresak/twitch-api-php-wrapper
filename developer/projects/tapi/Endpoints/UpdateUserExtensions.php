<?php

namespace TwitchAPI;

class UpdateUserExtensions extends Endpoint {
  public RequiredScopes() {
    return "user:edit:broadcast";
  }

  public RequestURL() {
    return "helix/users/extensions";
  }

  public CallEndpoint($mixedVariable) {

  }
}

?>
