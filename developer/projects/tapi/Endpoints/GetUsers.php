<?php

namespace TwitchAPI;

class GetUsers extends Endpoint {
  public RequiredScopes() {
    return "user:read:email";
  }

  public RequestURL() {
    return "helix/users";
  }

  public CallEndpoint($mixedVariable) {

  }
}

?>
