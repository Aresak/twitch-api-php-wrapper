<?php

namespace TwitchAPI;

abstract class Endpoint {
  public abstract RequiredScopes();
  public abstract RequestURL();

  public abstract CallEndpoint($mixedVariable);
}

class Settings {
  public static $client_id = "";
  public static $redirect_uri = "";
  public static $client_secret = "";
}

class OAuth {
  private $code = "";
  private $access_token = "";
  private $refresh_token = "";
  private $expires_in = 0;

  private $scopes = array();

  private $last_state = "";

  public function RedirectToAuthorize() {
    
  }


  public function getCode() {
    return $this->code;
  }

  public function getAccessToken() {
    return $this->access_token;
  }

  public function getRefreshToken() {
    return $this->refresh_token;
  }

  public function getExpire() {
    return $this->expires_in;
  }

  public function getLastState() {
    return $this->last_state;
  }

  public function setLastState($state) {
    $this->last_state = $state;
  }

  public function getScopes() {
    return $this->scopes;
  }

  public function getScopesAsString() {
    return join(" ", $this->scopes);
  }

  public function addScope($scope) {
    array_push($this->scopes, $scope);
  }

  public function removeScope($scope) {
    $this->scopes = array_diff($this->scopes, array($scope));
  }
}
?>
